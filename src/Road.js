import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    spaceV: {
        display: "block",
        backgroundColor: "#A9A9A9",
        float: "left",
        width: "40px",
        height: "80px",
    },
    spaceH: {
        display: "block",
        backgroundColor: "#A9A9A9",
        float: "left",
        width: "80px",
        height: "40px",
    },
};

function Road(props) {
    const { classes } = props;
    return (
        <div className={props.rotation === "H" ? classes.spaceH : classes.spaceV} />
    );
}

export default withStyles(styles)(Road);