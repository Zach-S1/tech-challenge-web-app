import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route } from "react-router-dom";


import './Reset.css';
import './Custom.css';
import Home from "./Home";
import AppBar from "./AppBar";

const styles = {
    menuPadding:{
        marginTop: '48px',
        height: 'calc(100vh - 48px)',
    }
};

function App(props) {
    const {classes} = props;
    return (
            <Router>
                <AppBar/>
                <div className={classes.menuPadding}>
                    <Route exact path="/" component={Home}/>
                </div>
            </Router>
    );
}

export default withStyles(styles)(App);