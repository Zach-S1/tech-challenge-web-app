import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Icon from "@material-ui/core/Icon";

const styles = {
    spaceV: {
        display: "block",
        border: "1px solid White",
        float: "left",
        width: "40px",
        height: "80px",
        color: "White",
    },
    spaceH: {
        display: "block",
        border: "1px solid White",
        float: "left",
        width: "80px",
        height: "40px",
        color: "White",
    }
};

function Space(props) {
    const { classes } = props;
    return (
        <div className={props.rotation === "H" ? classes.spaceH : classes.spaceV} style={{ backgroundColor: props.taken === true ? "Red" : "Green" }}><Icon>{props.type === 'disabled' ? "accessible" : props.type === 'electric' ? "ev_station" : props.type === 'space' ? "directions_car": ""}</Icon></div>
    );
}

export default withStyles(styles)(Space);