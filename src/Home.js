import React, {Component} from "react";
import {withStyles} from "@material-ui/core";
import Space from "./Space";
import Empty from "./Empty";
import Road from "./Road";

const styles = {

    main: {
        maxWidth: "720px",
        minWidth: "720px",
        maxHeight: "520px",
        minHeight: "520px",
        height: "500px",
        margin: "auto",
        paddingTop: "5%"
    }
};

class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {carParkData: null};
    };

    componentDidMount() {
            setInterval(() => fetch('http://ozdee.mocklab.io/test')
            .then(res => res.json())
            .then((data) => {
                this.setState({ carParkData: data.parkingSpaces })
            })
            .catch(console.log), 1000);
    }

    render(){
        const {classes} = this.props;
        var i = 0;
        const carParkMap = [
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,
            2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,
            2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,
        ];
        return(
            <div className={classes.main}>
                {carParkMap.map((value, index) => {
                    var test = {
                        "name": "",
                        "type": "",
                        "populated": true
                    };
                    var dataSpace;
                    if(this.state.carParkData != null){
                        dataSpace = this.state.carParkData.find((element) => {
                            return element.name == i + 1;
                        });
                    }
                    if(dataSpace == null){
                        dataSpace = test;
                    }
                    switch(value) {
                        case 0:
                            i++;
                            return <Space id={i} taken={dataSpace.populated} type={dataSpace.type} rotation={"V"}/>;
                        case 1:
                            return <Empty rotation={"V"}/>;
                        case 2:
                            return <Road rotation={"V"}/>;
                        case 3:
                            i++;
                            return <Space id={i} taken={dataSpace.populated} type={dataSpace.type} rotation={"H"}/>;
                        default:
                        // code block
                    }
                })}
            </div>
        );
    }
}

export default withStyles(styles)(Home);